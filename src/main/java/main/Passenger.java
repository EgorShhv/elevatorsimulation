package main;

public class Passenger {
    private int id;
    private int startFloor;
    private int targetFloor;

    public Passenger(int id, int startFloor, int targetFloor) {
        this.id = id;
        this.startFloor = startFloor;
        this.targetFloor = targetFloor;
    }

    public int getStartFloor() {
        return startFloor;
    }

    public void setStartFloor(int startFloor) {
        this.startFloor = startFloor;
    }

    public int getTargetFloor() {
        return targetFloor;
    }

    public void setTargetFloor(int targetFloor) {
        this.targetFloor = targetFloor;
    }

    public int getId() {
        return id;
    }
}
