/*
 * Идея решения задачи взята из обсуждения https://ru.stackoverflow.com/questions/1166073/Задача-симулятор-лифта
 *
 * */

package main;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Elevator {

    public enum STATE {
        MOVE_UP, // Движется вверх.
        MOVE_DOWN,  // Движется вниз.
        PENDING,    // Стоит на месте во время посадки/высадки пассажиров.
        SLEEP   // Нет целевых этажей, стоит в ожидании вызова.
    }

    private final int id;
    private final int countOfFloors;
    private final long floorInterval;
    private final long waitingInterval;
    private final int capacity;
    private int currentFloor = 0;
    private STATE state;
    private final TreeSet<Integer> targetFloors;
    private final List<Passenger> passengers;
    private final ExecutorService executor;
    private final Object lock;
    private final List<Queue<Passenger>> passengersOnFloors; // Состояние этажа на момент вызова.

    public Elevator(int id, int countOfFloors, long floorInterval, long waitingInterval, int capacity, List<Queue<Passenger>> passengersOnFloors) {

        this.passengersOnFloors = passengersOnFloors;
        this.id = id;

        this.countOfFloors = countOfFloors;
        this.floorInterval = floorInterval;
        this.waitingInterval = waitingInterval;

        this.capacity = capacity;

        targetFloors = new TreeSet<>();
        passengers = new ArrayList<>();

        state = STATE.SLEEP;

        passengersOnFloors = new ArrayList<>();
        for (int i = 0; i < countOfFloors; i++) {
            passengersOnFloors.add(new ArrayDeque<>());
        }

        this.executor = Executors.newSingleThreadExecutor();
        lock = new Object();


    }

    /*
    * Функция отвечает за движение лифта. Сначала проходятся все этажи вверх, затем вниз.
    * */
    public void move() {

        executor.submit(() -> {
            try {
                boolean isHasWork = this.state != STATE.SLEEP;
                while (isHasWork) {
                    TreeSet<Integer> tailSet;
                    TreeSet<Integer> headSet;

                    synchronized (lock) {
                        headSet = (TreeSet<Integer>) targetFloors.headSet(currentFloor);
                        tailSet = (TreeSet<Integer>) targetFloors.tailSet(currentFloor);
                    }

                    // Если есть целевые этажи выше текущего, то движемся наверх.
                    if (!tailSet.isEmpty()) this.moveUp();

                    // Если есть целевые этажи ниже текущего, то движемся вниз.
                    if (!headSet.isEmpty()) this.moveDown();

                    synchronized (lock) {
                        isHasWork = this.state != STATE.SLEEP;
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

    }

    /*
     * Вызов лифта с указанного этажа.
     * ВАЖНО!!! Нумерация этажей начинается с нуля !!!
     *
     * param: floor - номер этажа, с которого вызвали лифт (нумерация с нуля).
     * */
    public void callFromFloor(final int floor) {
        synchronized (lock) {
            targetFloors.add(floor);
            sendMessage("targetFloors: " + targetFloors);
        }
        if (this.state == STATE.SLEEP) {
            this.state = STATE.PENDING;
            move();
        }
    }

    public void callFromFloorPassenger(final int floor) {
        sendMessage("Call on floor " + (floor + 1));
        synchronized (lock) {
            targetFloors.add(floor);
            sendMessage("targetFloors: " + targetFloors);
        }

        if (this.state == STATE.SLEEP) {
            this.state = STATE.PENDING;
            move();
        }
    }

    /*
     * Функция добавления пассажира в лифт. Добавляет пассажира в массив пассажиров и помечает целевой этаж пассажира
     * как этаж для посещения.
     *
     * param: passenger - Пассажир с исходным и целевым этажами.
     * */
    public void addPassenger(Passenger passenger) {
        sendMessage(
                "New Passenger " + passenger.getId()
                        + ": from " + (passenger.getStartFloor() + 1)
                        + " to " + (passenger.getTargetFloor() + 1)
                        + ")"
        );
        passengers.add(passenger);
        targetFloors.add(passenger.getTargetFloor());

        if (this.state == STATE.SLEEP) {
            this.state = STATE.PENDING;
            move();
        }
    }

    /*
    * Функция движения лифта вверх.
    * */
    private void moveUp() throws InterruptedException {
        for (int i = currentFloor; !targetFloors.tailSet(currentFloor).isEmpty() && i < countOfFloors; i++) {

            state = STATE.MOVE_UP;

            moveTo(i);

            synchronized (lock) {
                if (targetFloors.contains(i)) {
                    targetFloors.remove(i);
                    makeAStop();
                }
            }
        }
    }

    /*
     * Функция движения лифта вниз.
     * */
    private void moveDown() throws InterruptedException {
        for (int i = currentFloor; !targetFloors.headSet(currentFloor).isEmpty() && i >= 0; i--) {

            state = STATE.MOVE_DOWN;

            moveTo(i);

            synchronized (lock) {
                if (targetFloors.contains(i)) {
                    targetFloors.remove(i);
                    makeAStop();
                }
            }
        }
    }

    /*
     * Функция перемещения лифта на указанный этаж.
     *
     * param: targetFloor - этаж, на который следует переместить лифт.
     * */
    private void moveTo(int targetFloor) throws InterruptedException {
        Thread.sleep(floorInterval);
        currentFloor = targetFloor;
        sendMessage("CURRENT FLOOR: " + (currentFloor + 1));

    }

    /*
    * Функция остановки лифта. Вызывает основные действия при остановке:
    * открытие/закрытие дверей, выпускает пассажиров, впускает пассажиров
    * */
    private void makeAStop() throws InterruptedException {
        doorOpenClose();
        letPassengersOut();
        putNewPassengers();

        if (targetFloors.isEmpty()) {
            this.state = STATE.SLEEP;
        }
    }

    /*
    * Функция, добавляющая пассажиров с текущего этажа в лифт.
    * */
    private void putNewPassengers() {
        while (capacity - passengers.size() > 0 && !passengersOnFloors.get(currentFloor).isEmpty()) {
            Passenger tmp = Objects.requireNonNull(passengersOnFloors.get(currentFloor).poll());
            addPassenger(tmp);
        }
    }

    /*
    * Функция, имитирующая открытие и закрытие дверей.
    * */
    private void doorOpenClose() throws InterruptedException {
        sendMessage("DOORS OPENED");
        state = STATE.PENDING;
        Thread.sleep(waitingInterval);
        sendMessage("DOORS CLOSED");
    }

    /*
     * Функция убирает из массива тех пассажиров, целевой этаж которых, совпадает с текущим этажом лифта.
     * */
    private void letPassengersOut() {
        int N = this.passengers.size();
        for (int i = N - 1; i >= 0; i--) {
            if (passengers.get(i).getTargetFloor() == this.currentFloor) {
                sendMessage("Passenger (" + passengers.get(i).getId() + ") is out");
                passengers.remove(i);
            }
        }
    }

    /*
     * Вспомогательная функция, которая выводит указанное сообщение на экран,
     * добавляя время и ID лифта в начало сообщения.
     * */
    private void sendMessage(String message) {
        System.out.println(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME) + " <" + this.id + "> " + message);
    }

    public int getCurrentFloor() {
        synchronized (lock) {
            return currentFloor;
        }
    }

    public STATE getState() {
        synchronized (lock) {
            return state;
        }
    }

    public int getNumberOfAvailableSeats() {
        return capacity - passengers.size();
    }

    public boolean isEmpty() {
        return passengers.isEmpty();
    }

    public boolean isFull() {
        return capacity == passengers.size();
    }

    public int getPassengersCount() {
        return passengers.size();
    }

    public int getCapacity() {
        return capacity;
    }

    public void stop() {
        executor.shutdown();
    }
}
