package main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimulationField extends JPanel implements ActionListener {
    private final int Y_SIZE;
    private final int ELEVATOR_SIZE;
    private final int LEFT_SIZE = 45;

    private Image elevatorImage;
    private Image LTImage;
    private Image CTImage;
    private Image RTImage;
    private Image LMImage;
    private Image CMImage;
    private Image RMImage;
    private Image LBImage;
    private Image CBImage;
    private Image RBImage;
    private Timer timer;

    private final int[] elevatorPositionX;
    private final int[] elevatorPositionY;
    private int[] elevatorPassengers;

    private final Build build;

    public boolean isRun = true;

    public SimulationField(int heightForm, Build build, int elevatorSize) {
        Y_SIZE = heightForm;

        this.build = build;


        elevatorPositionX = new int[build.getCountOfElevators()];
        elevatorPositionY = new int[build.getCountOfElevators()];
        elevatorPassengers = new int[build.getCountOfElevators()];

        ELEVATOR_SIZE = elevatorSize;

        setBackground(Color.DARK_GRAY);
        loadImages();

        initPainting();
        setFocusable(true);
    }

    public void initPainting() {
        int[] floors = build.getElevatorsFloors();
        for (int i = 0; i < build.getCountOfElevators(); i++) {
            elevatorPositionX[i] = LEFT_SIZE + i * (ELEVATOR_SIZE + 2);

            elevatorPositionY[i] = Y_SIZE - floors[i] * ELEVATOR_SIZE;
        }

        timer = new Timer(500, this);
        timer.start();
    }

    public void loadImages() {
        ImageIcon tmpIcon = new ImageIcon("images/elevator.png");
        elevatorImage = tmpIcon.getImage();

        tmpIcon = new ImageIcon("images/LT.png");
        LTImage = tmpIcon.getImage();

        tmpIcon = new ImageIcon("images/CT.png");
        CTImage = tmpIcon.getImage();

        tmpIcon = new ImageIcon("images/RT.png");
        RTImage = tmpIcon.getImage();

        tmpIcon = new ImageIcon("images/LM.png");
        LMImage = tmpIcon.getImage();

        tmpIcon = new ImageIcon("images/CM.png");
        CMImage = tmpIcon.getImage();

        tmpIcon = new ImageIcon("images/RM.png");
        RMImage = tmpIcon.getImage();

        tmpIcon = new ImageIcon("images/LB.png");
        LBImage = tmpIcon.getImage();

        tmpIcon = new ImageIcon("images/CB.png");
        CBImage = tmpIcon.getImage();

        tmpIcon = new ImageIcon("images/RB.png");
        RBImage = tmpIcon.getImage();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (isRun) {
            moveElevators();
        }
        repaint();
    }


    /*
    * Функция перемещения лифтов на форме.
    * */
    private void moveElevators() {

        int[] floors = build.getElevatorsFloors();
        for (int i = 0; i < build.getCountOfElevators(); i++) {
            elevatorPositionY[i] = Y_SIZE - floors[i] * ELEVATOR_SIZE;
        }
    }


    /*
    * Перезаписанная функция, которая перерисовывает компоненты форма. Вызывается по таймеру.
    * */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (isRun) {
            g.setColor(Color.WHITE);
            // Фон.
            // Первый этаж здания:
            int numberOfElevators = build.getCountOfElevators();
            int numberOfFloors = build.getCountOfFloors();

            // LB
            int BOTTOM_SIZE = 55;
            g.drawImage(LBImage, 0, Y_SIZE - BOTTOM_SIZE, this);

            // RB
            g.drawImage(RBImage, LEFT_SIZE + numberOfElevators * (ELEVATOR_SIZE + 2), Y_SIZE - BOTTOM_SIZE, this);

            for (int i = 0; i < numberOfFloors; i++) {
                if (i > 0) {
                    // LM
                    g.drawImage(LMImage, 0, Y_SIZE - (BOTTOM_SIZE + ELEVATOR_SIZE * i), this);
                    // RM
                    g.drawImage(RMImage, LEFT_SIZE + numberOfElevators * (ELEVATOR_SIZE + 2), Y_SIZE - (BOTTOM_SIZE + ELEVATOR_SIZE * i), this);
                }
                for (int j = 0; j < numberOfElevators; j++) {
                    if (i == 0) {
                        // CB
                        g.drawImage(CBImage, LEFT_SIZE + j * (ELEVATOR_SIZE + 2), Y_SIZE - BOTTOM_SIZE, this);
                    }
                    // CM
                    g.drawImage(CMImage, LEFT_SIZE + j * (ELEVATOR_SIZE + 2), Y_SIZE - (BOTTOM_SIZE + i * ELEVATOR_SIZE), this);
                }
            }
            // Top image
            int TOP_SIZE = 38;
            int top_y = Y_SIZE - (BOTTOM_SIZE + (numberOfFloors - 1) * ELEVATOR_SIZE + TOP_SIZE);
            // LT
            g.drawImage(LTImage, 0, top_y, this);
            // CT
            for (int i = 0; i < numberOfElevators; i++)
                g.drawImage(
                        CTImage,
                        LEFT_SIZE + i * (ELEVATOR_SIZE + 2),
                        top_y,
                        this
                );
            // RT
            g.drawImage(RTImage, LEFT_SIZE + numberOfElevators * (ELEVATOR_SIZE + 2), top_y, this);


            // ---------------------------- Рисование лифтов. ----------------------------
            elevatorPassengers = build.getCountPassengersInElevators();

            for (int i = 0; i < numberOfElevators; i++) {

                g.drawImage(elevatorImage, elevatorPositionX[i], elevatorPositionY[i] - 10, this);


                String countPassengers = String.valueOf(elevatorPassengers[i]);
                g.drawString(countPassengers, elevatorPositionX[i] + ELEVATOR_SIZE / 2, elevatorPositionY[i] - 10);
            }
            // ---------------------------------------------------------------------------

            // ---------------------------- Рисование этажей. ----------------------------
            g.setColor(Color.lightGray);

            int startFloorX = 0;
            int startFloorY = Y_SIZE;

            g.drawLine(startFloorX, startFloorY, 50, startFloorY);

            for (int i = 0; i < build.getCountOfFloors(); i++) {

                g.drawString(
                        String.valueOf(i + 1), LEFT_SIZE - 10, Y_SIZE - (i * ELEVATOR_SIZE) - ELEVATOR_SIZE / 2);
            }
            // ---------------------------------------------------------------------------
        }
    }
}
