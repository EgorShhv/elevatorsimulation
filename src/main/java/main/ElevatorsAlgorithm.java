package main;

public class ElevatorsAlgorithm {

    /*
     * Функция реализует простой алгоритм выбора ближайшего лифта.
     * Ближайшим считается лифт, который находится ближе всего к вызванному этажу И
     * либо находится в ожидании пассажиров, либо двигается в сторону вызываемого этажа.
     * Если лифтов, удовлетворяющим такому условию нет, возвращается первый лифт.
     *
     * param: elevatorsStates - массив состояний (Elevator.STATE) для каждого лифта соответственно.
     * param: elevatorsFloors - массив этажей (int), на которых в данный момент находятся лифты соответственно.
     * param: callFloor - номер этажа, для которого нужно вызвать лифт.
     *
     * return: int nearestElevator - номер лифта, который следует вызвать для заданного этажа.
     * */
    public static int nearestElevatorWithSameDirectionTravel(
            Elevator.STATE[] elevatorsStates,
            int[] elevatorsFloors,
            int[] availableStates,
            int callFloor
    ) {
        int nearestElevatorIndex = 0;
        int distance = Integer.MAX_VALUE;
        int elevatorsCount = elevatorsFloors.length;

        for (int i = 0; i < elevatorsCount; i++) {
            if (elevatorsFloors[i] == callFloor) {
                return i;
            }
            if (availableStates[i] > 0 &&
                    (elevatorsStates[i] == Elevator.STATE.SLEEP
                            || (callFloor < elevatorsFloors[i] && elevatorsStates[i] == Elevator.STATE.MOVE_DOWN)
                            || (callFloor > elevatorsFloors[i] && elevatorsStates[i] == Elevator.STATE.MOVE_UP))
            ) {
                int tmpDistance = Math.abs(elevatorsFloors[i] - callFloor);
                if (tmpDistance < distance) {
                    distance = tmpDistance;
                    nearestElevatorIndex = i;
                }
            }
        }

        System.out.println("Вызван лифт: " + nearestElevatorIndex);
        return nearestElevatorIndex;
    }
}
