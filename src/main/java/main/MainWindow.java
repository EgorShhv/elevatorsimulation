package main;

import javax.swing.*;
import java.util.Scanner;

public class MainWindow extends JFrame {

    private static final int TOP_IMAGE_HEIGHT = 38;
    private static final int BOTTOM_IMAGE_HEIGHT = 55;
    private static final int LEFT_IMAGE_WEIGHT = 38;
    private static final int RIGHT_IMAGE_WEIGHT = 230;
    private static final int ELEVATOR_IMAGE_SIZE = 45; // Размер стороны квадрата изображения лифта (px).
    private static final int WINDOW_TITLE_BAR_HEIGHT = 32;    // Высота строки заголовка окна (px).


    public MainWindow(int weight, int height, SimulationField simulationField) {

        setTitle("Лифтовая");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setSize(weight, height);
        setResizable(false);

        add(simulationField);

        setVisible(true);
    }

    public static void main(String[] args) {
        // =================== Инициализация всех необходимых для работы параметров ===================

        System.out.println();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input mode (1 - manual operation. 2 or Other - automatic mode): ");
        int mode = scanner.nextInt();

        System.out.println("Input number of floors (int): ");
        int numOfFloors = scanner.nextInt();

        System.out.println("Input number of elevators (int): ");
        int numOfElevators = scanner.nextInt();

        System.out.println("Input floor height (m) (double): ");
        double floorHeight = scanner.nextDouble();

        System.out.println("Input floor velocity (m/s) (double): ");
        double floorVelocity = scanner.nextDouble();

        System.out.println("Input floor doors time (seconds) (int): ");
        long waitingInterval = scanner.nextLong() * 1000;
        long floorTime = (long) (1000.0 * floorHeight / floorVelocity);

        int capacity = 10;

        Build build = new Build(numOfFloors, numOfElevators, floorTime, waitingInterval, capacity);

        int weightForm = LEFT_IMAGE_WEIGHT + RIGHT_IMAGE_WEIGHT + (ELEVATOR_IMAGE_SIZE + 2) * numOfElevators;
        int heightForm = TOP_IMAGE_HEIGHT + BOTTOM_IMAGE_HEIGHT + numOfFloors * ELEVATOR_IMAGE_SIZE;
        System.out.println(weightForm + "; " + heightForm);
        SimulationField simulationField = new SimulationField(
                heightForm - WINDOW_TITLE_BAR_HEIGHT,
                build,
                ELEVATOR_IMAGE_SIZE
        );

        // ============================================================================================

        try {
            Thread myThread;
            if (mode == 1) {
                myThread = new Thread(build::startControlMode, "MyThread");
            } else {
                System.out.println("Enter the number of people (int): ");
                int countOfPeople = scanner.nextInt();

                Runnable r = () -> build.startSimulationMode(countOfPeople);
                myThread = new Thread(r, "MyThread");
            }
            myThread.start();

            MainWindow mw = new MainWindow(
                    weightForm,
                    heightForm + WINDOW_TITLE_BAR_HEIGHT,
                    simulationField);

            myThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
