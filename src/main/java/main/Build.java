package main;

import java.util.*;

import static main.ElevatorsAlgorithm.nearestElevatorWithSameDirectionTravel;

public class Build {

    /*
     * TODO: 1) Проверить функцию Build.startControlMode().
     *
     * TODO: 2) Написать функцию Build.startSimulationMode().
     * */

    private final int countOfFloors;
    private final List<Queue<Passenger>> passengersOnFloors;
    private final List<Elevator> elevators;

    public Build(int countOfFloors, int countOfElevators, long floorInterval, long waitingInterval, int elevatorsCapacity) {
        this.countOfFloors = countOfFloors;

        this.passengersOnFloors = new ArrayList<>();
        for (int i = 0; i < countOfFloors; i++)
            this.passengersOnFloors.add(new ArrayDeque<>());

        this.elevators = new ArrayList<>();

        for (int i = 0; i < countOfElevators; i++)
            this.elevators.add(
                    new Elevator(
                            i,
                            countOfFloors,
                            floorInterval,
                            waitingInterval,
                            elevatorsCapacity,
                            this.passengersOnFloors)
            );
    }

    /*
     * Функция запускает режим симуляции работы лифтов. Случайным образом создаются новые пассажиры
     * и сами вызывают лифты на случайные этажи.
     * */
    public void startSimulationMode(int countOfPeople) {

        try {
            int peopleCounter = 0;
            while (peopleCounter < countOfPeople) {

                int callFloor = new Random().nextInt(0, this.countOfFloors);

                int targetFloor = callFloor;
                // Гарантируем вызов на другой этаж
                while (targetFloor == callFloor)
                    targetFloor = new Random().nextInt(0, this.countOfFloors);

                int elevatorIndex = nearestElevatorWithSameDirectionTravel(
                        getElevatorsState(),
                        getElevatorsFloors(),
                        getElevatorsAvailableSeats(),
                        callFloor
                );

                passengersOnFloors.get(callFloor).add(new Passenger(peopleCounter, callFloor, targetFloor));


                elevators.get(elevatorIndex).callFromFloorPassenger(callFloor);


                peopleCounter++;
                Thread.sleep(5000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /*
     * Функция начинает режим ручного ввода целевых этажей. Порядок вызова не является порядком следования лифта.
     * */
    public void startControlMode() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter each target floors at new line. Enter 'q' to exit: ");

        while (true) {
            System.out.println("Ваш этаж?:");
            String line = scanner.nextLine();

            if ("q".equals(line)) {
                stopAllElevators();
                return;
            }
            try {
                int nextFloor = Integer.parseInt(line);

                if (nextFloor <= 0 || nextFloor > countOfFloors) throw new Exception("Wrong number of floor");

                int elevatorIndex = nearestElevatorWithSameDirectionTravel(getElevatorsState(), getElevatorsFloors(), getElevatorsAvailableSeats(), nextFloor);

                elevators.get(elevatorIndex).callFromFloor(nextFloor - 1);
            } catch (Exception e) {
                System.out.println(line + "not a number");
            }
        }
    }

    private void stopAllElevators() {
        for (Elevator elevator : elevators) {
            elevator.stop();
        }
    }

    /*
     * Функция показывает, в каких состояниях находятся лифты в данный момент.
     * */
    public int[] getElevatorsFloors() {
        int elevatorsCount = elevators.size();

        int[] result = new int[elevatorsCount];
        for (int i = 0; i < elevatorsCount; i++) {
            result[i] = elevators.get(i).getCurrentFloor() + 1;
        }

        return result;
    }

    /*
     * Функция показывает, сколько свободных мест в каждом лифте.
     * */
    public int[] getElevatorsAvailableSeats() {
        int elevatorsCount = elevators.size();

        int[] result = new int[elevatorsCount];
        for (int i = 0; i < elevatorsCount; i++) {
            result[i] = elevators.get(i).getCapacity() - elevators.get(i).getPassengersCount();
        }
        return result;
    }

    /*
     * Функция показывает, на каких этажах находятся лифты в данный момент.
     * */
    public Elevator.STATE[] getElevatorsState() {
        int elevatorsCount = elevators.size();

        Elevator.STATE[] result = new Elevator.STATE[elevatorsCount];
        for (int i = 0; i < elevatorsCount; i++) {
            result[i] = elevators.get(i).getState();
        }

        return result;
    }

    /*
     * Функция показывает, сколько людей находится на каждом этаже в ожидании лифта.
     * */
    public int[] getCountPassengersOnFloors() {
        int[] result = new int[countOfFloors];
        for (int i = 0; i < countOfFloors; i++) {
            result[i] = passengersOnFloors.get(i).size();
        }
        return result;
    }

    /*
     * Функция показывает, сколько людей находится в каждом лифте.
     * */
    public int[] getCountPassengersInElevators() {
        int elevatorsCount = elevators.size();

        int[] result = new int[elevatorsCount];
        for (int i = 0; i < elevatorsCount; i++) {
            result[i] = elevators.get(i).getPassengersCount();
        }

        return result;
    }

    public int getCountOfFloors() {
        return countOfFloors;
    }

    public int getCountOfElevators() {
        return elevators.size();
    }
}
